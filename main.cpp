#include <iostream>
#include "Menu.h"

using namespace std;

int main()
{
    bool exit_prog = false;
    bool error_input = false;
    int command;
    system ("mode con cols=195 lines=30");
    setlocale(LC_ALL, "rus");
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    system("color F0");
    while (!exit_prog)
    {
        system("cls");
        cout << "\t\t\t\t\t\t�������� ������ �������� ��I� ��. �I-14-1" << endl;
        cout << "\t\t\t\t\t\t���� ��� �������-�����i�������� �i���������" << endl;
        cout << "������� ����:\n1. ������ ������\n2. �����" << endl;
        if (error_input)
        {
            cout << endl << "����������� ������� �������!" << endl << endl;
            error_input = false;
        }
        cout << "�����i�� �������:\n\n>>";
        cin.clear();
        cin.sync();
        cin >> command;
        switch (command)
        {
        default:
            error_input = true;
            continue;
        case 1:
            {
                Menu *menu = new Menu();
                menu->runFile();
                delete menu;
                break;
            }

        case 2:
            exit_prog = true;
            break;
        }
    }
    return 0;
}
