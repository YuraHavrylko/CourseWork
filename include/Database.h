#ifndef DATABASE_H
#define DATABASE_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <clocale>
#include <windows.h>
#include <algorithm>
#include <iomanip>
#include <stdio.h>

using namespace std;

class Database
{
    public:
    Database();
    ~Database();

    void clearArrays();

    void openFile();
    void createFile();
    void deleteFile();

    void getScreenCustomers();
    void getScreenDelivery();
    void getScreenEmployees();
    void getScreenManufacturerProduct();
    void getScreenOrder();
    void getScreenOrderedProduct();
    void getScreenProduct();
    void getScreenTypeProduct();

    void saveDataBase();
    void saveFileCustomers();
    void saveFileDelivery();
    void saveFileEmployees();
    void saveFileManufacturerProduct();
    void saveFileOrder();
    void saveFileOrderedProduct();
    void saveFileProduct();
    void saveFileTypeProduct();

    void loadDataBase();
    void loadFileCustomers();
    void loadFileDelivery();
    void loadFileEmployees();
    void loadFileManufacturerProduct();
    void loadFileOrder();
    void loadFileOrderedProduct();
    void loadFileProduct();
    void loadFileTypeProduct();

    void addCustomers();
    void addDelivery();
    void addEmployees();
    void addManufacturerProduct();
    void addOrder();
    void addOrderedProduct();
    void addProduct();
    void addTypeProduct();

    void editCustomers(int);
    void editDelivery(int);
    void editEmployees(int);
    void editManufacturerProduct(int);
    void editOrder(int);
    void editOrderedProduct(int);
    void editProduct(int);
    void editTypeProduct(int);

    void deleteCustomers(int);
    void deleteDelivery(int);
    void deleteEmployees(int);
    void deleteManufacturerProduct(int);
    void deleteOrder(int);
    void deleteOrderedProduct(int);
    void deleteProduct(int);
    void deleteTypeProduct(int);

    void sortCustomers(int);
    void sortDelivery(int);
    void sortEmployees(int);
    void sortManufacturerProduct(int);
    void sortOrder(int);
    void sortOrderedProduct(int);
    void sortProduct(int);
    void sortTypeProduct(int);

    void queryPosition();
    void quetyPriceUpper();
    void quetyProductIs();
    void quetyIsPost();
    //////////////////////////////////////////////////////////////////////
    fstream DBFile;
    ifstream configFile;
    string nameFile;
    struct DBconfig
    {
        string name;
        vector <string> name_pole;
    };
    DBconfig myConfig[8];
    //////////////////////////////////////////////////////////////////////
    struct typeProduct // ���� ������
    {
        int Iid;
        string Scategory;
        string Sorder;
    };
    vector <typeProduct> tableTypeProduct;
    //////////////////////////////////////////////////////////////////////
    struct manufacturerProduct // �������� ������
    {
        int Iid;
        string Sname;
        string Sadress;
        string Scity;
        string Scountry;
        string Stelephone;
        string Shome_page;
    };
    vector <manufacturerProduct> tableManufacturerProduct;
     //////////////////////////////////////////////////////////////////////
    struct product // �����
    {
        int Iid;
        string Stype;
        string Smanufacturer;
        string Sbrand;
        double Dprice;
        int Iamount;
        string Sunit_of_measurement;
        string Ssupply;
    };
    vector <product> tableProduct;
    //////////////////////////////////////////////////////////////////////
    struct employees // �����������
    {
        int Iid;
        string Sfirst_name;
        string Slast_name;
        string Sposition;
        string Sdata;
        string Sadress;
        string Scity;
        string Stelephone;
    };
    vector <employees> tableEmployees;
    //////////////////////////////////////////////////////////////////////
    struct orderedProduct // ��������� ������
    {
        int Iid;
        string Sproduct;
        double Dprice;
        int Iamount;
    };
    vector <orderedProduct> tableOderedProduct;
    //////////////////////////////////////////////////////////////////////
    struct delivery // ��������
    {
        int Iid;
        string Sname;
        string Stelephone;
    };
    vector <delivery> tableDelivery;
    //////////////////////////////////////////////////////////////////////
    struct customers // �볺���
    {
        string Sid;
        string Sname;
        string Sadres;
        string Scity;
        string Scountry;
        string Stelephone;
    };
    vector <customers> tableCustomers;
    //////////////////////////////////////////////////////////////////////
    struct order
    {
        int Iid;
        string Sclient;
        string Semployees;
        string Sdate_of_appointment;
        string Sexecution_date;
        string Sdelivery;
        double Dcost_delivery;
        string Sname_recipient;
        string Sadress_recipient;
        string Scity_recipient;
        string Scountry_recipient;
    };
    vector <order> tableOrder;
};

#endif // DATABASE_H
