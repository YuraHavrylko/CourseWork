#ifndef MENU_H
#define MENU_H

#include "Database.h"

class Menu
{
public:
    Database* database;

    Menu();
    ~Menu();

    void runMenu();
    void runFile();

    void workTables();

    void queries();
    void query1();
    void query2();


    void workCustomers();
    void workDelidery();
    void workEmployees();
    void workManufacturerProduct();
    void workOrder();
    void workOrderedProduct();
    void workProduct();
    void workTypeProduct();
};

#endif // MENU_H
