#include "Menu.h"

Menu::Menu()
{

}

Menu::~Menu()
{
    delete database;
}

void Menu::runFile()
{
    database = new Database();
    bool exit_prog = false;
    bool error_input = false;
    int command;
    while (!exit_prog)
    {
        system("cls");
        cout << "\t\t\t\t\t\t������ � ������ �����" << endl;
        cout << "������� ����:\n1. ����������� ���� �����\n2. �������� ���� �����\n3. �������� ���� �����\n4. ���i�\n"  << endl;
        if (error_input)
        {
            cout << endl << "����������� ������� �������!" << endl << endl;
            error_input = false;
        }
        cout << "�����i�� �������:\n\n>>";
        cin.clear();
        cin.sync();
        cin >> command;
        switch (command)
        {
        default:
            error_input = true;
            continue;
        case 1:
            database->openFile();
            runMenu();
            break;
        case 2:
            database->createFile();
            break;
        case 3:
            database->deleteFile();
            break;
        case 4:
            exit_prog = true;
            break;
        }
    }
}

void Menu::runMenu()
{

    database->loadDataBase();
    bool exit_prog = false;
    bool error_input = false;
    int command;
    while (!exit_prog)
    {
        system("cls");
        cout << "\t\t\t\t\t\t������ � ������" << endl;
        cout << "������� ����:\n1. ������ � ���������\n\n2. ������\n\n3. �������� ���� �����\n\n4. �����" << endl;
        if (error_input)
        {
            cout << endl << "����������� ������� �������!" << endl << endl;
            error_input = false;
        }
        cout << "�����i�� �������:\n\n>>";
        cin.clear();
        cin.sync();
        cin >> command;
        switch (command)
        {
        default:
            error_input = true;
            continue;
        case 1:
            workTables();
            break;
        case 2:
            queries();
            break;
        case 3:
            break;
        case 4:
            break;
        case 5:
            database->saveDataBase();
            break;
        case 6:
            exit_prog = true;
            break;
        }
    }
    database->clearArrays();
}

void Menu::queries()
{
    bool exit_prog = false;
    bool error_input = false;
    int command;
    while (!exit_prog)
    {
        system("cls");
        cout << "\t\t\t\t\t\t������" << endl;
        cout << "1. �i���� ������������ �i���" << endl;
        cout << "2. �i�� ������ ���� ��������" << endl;
        cout << "3. �����i��� ������ �� �����i" << endl;
        cout << "4. �������� ������" << endl;
        cout << "5" << ". " << "���i�" <<endl;
        ///////
        if (error_input)
        {
            cout << endl << "����������� ������� �������!" << endl << endl;
            error_input = false;
        }
        cout << "�����i�� �������:\n\n>>";
        cin.clear();
        cin.sync();
        cin >> command;
        switch (command)
        {
        default:
            error_input = true;
            continue;
        case 1:
            system("cls");
            database->queryPosition();
            cin.get();
            cin.get();
            break;
        case 2:
            system("cls");
            database->quetyPriceUpper();
            cin.get();
            cin.get();
            break;
        case 3:
            system("cls");
            database->quetyProductIs();
            cin.get();
            cin.get();
            break;
        case 4:
            system("cls");
            database->quetyIsPost();
            cin.get();
            cin.get();
            break;
        case 5:
            exit_prog = true;
            break;
        }
    }
}


void Menu::workTables()
{
    bool exit_prog = false;
    bool error_input = false;
    int command;
    while (!exit_prog)
    {
        system("cls");
        cout << "\t\t\t\t\t\t������ � ���������" << endl;
        cout << "������i:" << endl;
        const int numberOfTables = 8;
        for (int i = 0; i < numberOfTables; i++)
        {
            cout << i + 1 << ". " << database->myConfig[i].name << endl;
        }
        cout << numberOfTables + 1 << ". " << "���i�" <<endl;
        if (error_input)
        {
            cout << endl << "����������� ������� �������!" << endl << endl;
            error_input = false;
        }
        cout << "�����i�� �������:\n\n>>";
        cin.clear();
        cin.sync();
        cin >> command;
        switch (command)
        {
        default:
            error_input = true;
            continue;
        case 1:
            workCustomers();
            break;
        case 2:
            workDelidery();
            break;
        case 3:
            workEmployees();
            break;
        case 4:
            workManufacturerProduct();
            break;
        case 5:
            workOrder();
            break;
        case 6:
            workOrderedProduct();
            break;
        case 7:
            workProduct();
            break;
        case 8:
            workTypeProduct();
            break;
        case 9:
            exit_prog = true;
            break;
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////


void Menu::workCustomers()
{
    bool exit_prog = false;
    bool error_input = false;
    int command;
    while (!exit_prog)
    {
        system("cls");
        cout << "�������: " << database->myConfig[0].name << endl;
        for (unsigned int i = 0; i < database->myConfig[0].name_pole.size(); i++)
        {
            cout << setw(25) << database->myConfig[0].name_pole[i] << setw(25);
        }
        cout << endl;
        database->getScreenCustomers();
        cout << "\n\n";
        cout << "1. ������ �������\n2. ���������� �������\n3. �������� �������\n4. ����������\n\n5. ���i�" << endl;
        if (error_input)
        {
            cout << endl << "����������� ������� �������!" << endl << endl;
            error_input = false;
        }
        cout << "�����i�� �������:\n\n>>";
        cin.clear();
        cin.sync();
        cin >> command;
        switch (command)
        {
        default:
            error_input = true;
            continue;
        case 1:
            system("cls");
            database->addCustomers();
            break;
        case 2:
            {
                int number;
                do
                {
                    cout << "����i�� ����� �������� ���� �����i��� ����������: " << endl;
                }
                while((cin >> number) && ((number < 1) || ((unsigned int)number > database->tableCustomers.size())));
                database->editCustomers(number - 1);
            }
            break;
        case 3:
            {
                int number;
                do
                {
                    cout << "����i�� ����� �������� ���� �����i��� ��������: " << endl;
                }
                while((cin >> number) && ((number < 1) || ((unsigned int)number > database->tableCustomers.size())));
                database->deleteCustomers(number - 1);
            }
            break;
        case 4:
            {
                system("cls");
                int temp1 = 0;
                int temp2 = 0;
                cout << "� �������:\n1. ���������\n2. ��������" << endl;
                while ((temp1 != 1) && (temp1 != 2))
                {
                    cout << "�����i�� �������:\n\n>>";
                    cin >> temp1;
                }
                system("cls");
                cout << "��������� ��:" << endl;
                for (unsigned int i = 0; i < database->myConfig[0].name_pole.size(); i++)
                    cout << i + 1 << ". " << database->myConfig[0].name_pole[i] << endl;
                while ((temp2 < 1) || ((unsigned int)temp1 > database->myConfig[0].name_pole.size()))
                {
                    cout << "�����i�� �������:\n\n>>";
                    cin >> temp2;
                }
                database->sortCustomers(temp2);
                if (temp1 == 2)
                    reverse(database->tableCustomers.begin(), database->tableCustomers.end());

            }
            break;
        case 5:
            exit_prog = true;
            break;
        }
    }
}

void Menu::workDelidery()
{
    bool exit_prog = false;
    bool error_input = false;
    int command;
    while (!exit_prog)
    {
        system("cls");
        cout << "�������: " << database->myConfig[1].name << endl;
        for (unsigned int i = 0; i < database->myConfig[1].name_pole.size(); i++)
        {
            cout << setw(25) << database->myConfig[1].name_pole[i] << setw(25);
        }
        cout << endl;
        database->getScreenDelivery();
        cout << "\n\n";
        cout << "1. ������ �������\n2. ���������� �������\n3. �������� �������\n4. ����������\n\n5. ���i�" << endl;
        if (error_input)
        {
            cout << endl << "����������� ������� �������!" << endl << endl;
            error_input = false;
        }
        cout << "�����i�� �������:\n\n>>";
        cin.clear();
        cin.sync();
        cin >> command;
        switch (command)
        {
        default:
            error_input = true;
            continue;
        case 1:
            system("cls");
            database->addDelivery();
            break;
        case 2:
            {
                int number;
                do
                {
                    cout << "����i�� ����� �������� ���� �����i��� ����������: " << endl;
                }
                while((cin >> number) && ((number < 1) || ((unsigned int)number > database->tableDelivery.size())));
                database->editDelivery(number - 1);
            }
            break;
        case 3:
            {
                int number;
                do
                {
                    cout << "����i�� ����� �������� ���� �����i��� ��������: " << endl;
                }
                while((cin >> number) && ((number < 1) || ((unsigned int)number > database->tableDelivery.size())));
                database->deleteDelivery(number - 1);
            }
            break;
        case 4:
            {
                system("cls");
                int temp1 = 0;
                int temp2 = 0;
                cout << "� �������:\n1. ���������\n2. ��������" << endl;
                while ((temp1 != 1) && (temp1 != 2))
                {
                    cout << "�����i�� �������:\n\n>>";
                    cin >> temp1;
                }
                system("cls");
                cout << "��������� ��:" << endl;
                for (unsigned int i = 0; i < database->myConfig[1].name_pole.size(); i++)
                    cout << i + 1 << ". " << database->myConfig[1].name_pole[i] << endl;
                while ((temp2 < 1) || ((unsigned int)temp1 > database->myConfig[1].name_pole.size()))
                {
                    cout << "�����i�� �������:\n\n>>";
                    cin >> temp2;
                }
                database->sortDelivery(temp2);
                if (temp1 == 2)
                    reverse(database->tableDelivery.begin(), database->tableDelivery.end());

            }
            break;
        case 5:
            exit_prog = true;
            break;
        }
    }
}

void Menu::workEmployees()
{
    bool exit_prog = false;
    bool error_input = false;
    int command;
    while (!exit_prog)
    {
        system("cls");
        cout << "�������: " << database->myConfig[2].name << endl;
        for (unsigned int i = 0; i < database->myConfig[2].name_pole.size(); i++)
        {
            cout << setw(23) << database->myConfig[2].name_pole[i] << setw(23);
        }
        cout << endl;
        database->getScreenEmployees();
        cout << "\n\n";
        cout << "1. ������ �������\n2. ���������� �������\n3. �������� �������\n4. ����������\n\n5. ���i�" << endl;
        if (error_input)
        {
            cout << endl << "����������� ������� �������!" << endl << endl;
            error_input = false;
        }
        cout << "�����i�� �������:\n\n>>";
        cin.clear();
        cin.sync();
        cin >> command;
        switch (command)
        {
        default:
            error_input = true;
            continue;
        case 1:
            system("cls");
            database->addEmployees();
            break;
        case 2:
            {
                int number;
                do
                {
                    cout << "����i�� ����� �������� ���� �����i��� ����������: " << endl;
                }
                while((cin >> number) && ((number < 1) || ((unsigned int)number > database->tableEmployees.size())));
                database->editEmployees(number - 1);
            }
            break;
        case 3:
            {
                int number;
                do
                {
                    cout << "����i�� ����� �������� ���� �����i��� ��������: " << endl;
                }
                while((cin >> number) && ((number < 1) || ((unsigned int)number > database->tableEmployees.size())));
                database->deleteEmployees(number - 1);
            }
            break;
        case 4:
            {
                system("cls");
                int temp1 = 0;
                int temp2 = 0;
                cout << "� �������:\n1. ���������\n2. ��������" << endl;
                while ((temp1 != 1) && (temp1 != 2))
                {
                    cout << "�����i�� �������:\n\n>>";
                    cin >> temp1;
                }
                system("cls");
                cout << "��������� ��:" << endl;
                for (unsigned int i = 0; i < database->myConfig[2].name_pole.size(); i++)
                    cout << i + 1 << ". " << database->myConfig[2].name_pole[i] << endl;
                while ((temp2 < 1) || ((unsigned int)temp1 > database->myConfig[2].name_pole.size()))
                {
                    cout << "�����i�� �������:\n\n>>";
                    cin >> temp2;
                }
                database->sortEmployees(temp2);
                if (temp1 == 2)
                    reverse(database->tableEmployees.begin(), database->tableEmployees.end());

            }
            break;
        case 5:
            exit_prog = true;
            break;
        }
    }
}

void Menu::workManufacturerProduct()
{
    bool exit_prog = false;
    bool error_input = false;
    int command;
    while (!exit_prog)
    {
        system("cls");
        cout << "�������: " << database->myConfig[3].name << endl;
        for (unsigned int i = 0; i < database->myConfig[3].name_pole.size(); i++)
        {
            cout << setw(25) << database->myConfig[3].name_pole[i] << setw(25);
        }
        cout << endl;
        database->getScreenManufacturerProduct();
        cout << "\n\n";
        cout << "1. ������ �������\n2. ���������� �������\n3. �������� �������\n4. ����������\n\n5. ���i�" << endl;
        if (error_input)
        {
            cout << endl << "����������� ������� �������!" << endl << endl;
            error_input = false;
        }
        cout << "�����i�� �������:\n\n>>";
        cin.clear();
        cin.sync();
        cin >> command;
        switch (command)
        {
        default:
            error_input = true;
            continue;
        case 1:
            system("cls");
            database->addManufacturerProduct();
            break;
        case 2:
            {
                int number;
                do
                {
                    cout << "����i�� ����� �������� ���� �����i��� ����������: " << endl;
                }
                while((cin >> number) && ((number < 1) || ((unsigned int)number > database->tableManufacturerProduct.size())));
                database->editManufacturerProduct(number - 1);
            }
            break;
        case 3:
            {
                int number;
                do
                {
                    cout << "����i�� ����� �������� ���� �����i��� ��������: " << endl;
                }
                while((cin >> number) && ((number < 1) || ((unsigned int)number > database->tableManufacturerProduct.size())));
                database->deleteManufacturerProduct(number - 1);
            }
            break;
        case 4:
            {
                system("cls");
                int temp1 = 0;
                int temp2 = 0;
                cout << "� �������:\n1. ���������\n2. ��������" << endl;
                while ((temp1 != 1) && (temp1 != 2))
                {
                    cout << "�����i�� �������:\n\n>>";
                    cin >> temp1;
                }
                system("cls");
                cout << "��������� ��:" << endl;
                for (unsigned int i = 0; i < database->myConfig[3].name_pole.size(); i++)
                    cout << i + 1 << ". " << database->myConfig[3].name_pole[i] << endl;
                while ((temp2 < 1) || ((unsigned int)temp1 > database->myConfig[3].name_pole.size()))
                {
                    cout << "�����i�� �������:\n\n>>";
                    cin >> temp2;
                }
                database->sortManufacturerProduct(temp2);
                if (temp1 == 2)
                    reverse(database->tableManufacturerProduct.begin(), database->tableManufacturerProduct.end());

            }
            break;
        case 5:
            exit_prog = true;
            break;
        }
    }
}

void Menu::workOrder()
{
    bool exit_prog = false;
    bool error_input = false;
    int command;
    while (!exit_prog)
    {
        system("cls");
        cout << "�������: " << database->myConfig[4].name << endl;
        for (unsigned int i = 0; i < database->myConfig[4].name_pole.size(); i++)
        {
            cout << setw(15) << database->myConfig[4].name_pole[i] << setw(15);
        }
        cout << endl;
        database->getScreenOrder();
        cout << "\n\n";
        cout << "1. ������ �������\n2. ���������� �������\n3. �������� �������\n4. ����������\n\n5. ���i�" << endl;
        if (error_input)
        {
            cout << endl << "����������� ������� �������!" << endl << endl;
            error_input = false;
        }
        cout << "�����i�� �������:\n\n>>";
        cin.clear();
        cin.sync();
        cin >> command;
        switch (command)
        {
        default:
            error_input = true;
            continue;
        case 1:
            system("cls");
            database->addOrder();
            break;
        case 2:
            {
                int number;
                do
                {
                    cout << "����i�� ����� �������� ���� �����i��� ����������: " << endl;
                }
                while((cin >> number) && ((number < 1) || ((unsigned int)number > database->tableOrder.size())));
                database->editOrder(number - 1);
            }
            break;
        case 3:
            {
                int number;
                do
                {
                    cout << "����i�� ����� �������� ���� �����i��� ��������: " << endl;
                }
                while((cin >> number) && ((number < 1) || ((unsigned int)number > database->tableOrder.size())));
                database->deleteOrder(number - 1);
            }
            break;
        case 4:
            {
                system("cls");
                int temp1 = 0;
                int temp2 = 0;
                cout << "� �������:\n1. ���������\n2. ��������" << endl;
                while ((temp1 != 1) && (temp1 != 2))
                {
                    cout << "�����i�� �������:\n\n>>";
                    cin >> temp1;
                }
                system("cls");
                cout << "��������� ��:" << endl;
                for (unsigned int i = 0; i < database->myConfig[4].name_pole.size(); i++)
                    cout << i + 1 << ". " << database->myConfig[4].name_pole[i] << endl;
                while ((temp2 < 1) || ((unsigned int)temp1 > database->myConfig[4].name_pole.size()))
                {
                    cout << "�����i�� �������:\n\n>>";
                    cin >> temp2;
                }
                database->sortOrder(temp2);
                if (temp1 == 2)
                    reverse(database->tableOrder.begin(), database->tableOrder.end());

            }
            break;
        case 5:
            exit_prog = true;
            break;
        }
    }
}

void Menu::workOrderedProduct()
{
    bool exit_prog = false;
    bool error_input = false;
    int command;
    while (!exit_prog)
    {
        system("cls");
        cout << "�������: " << database->myConfig[5].name << endl;
        for (unsigned int i = 0; i < database->myConfig[5].name_pole.size(); i++)
        {
            cout << setw(25) << database->myConfig[5].name_pole[i] << setw(25);
        }
        cout << endl;
        database->getScreenOrderedProduct();
        cout << "\n\n";
        cout << "1. ������ �������\n2. ���������� �������\n3. �������� �������\n4. ����������\n\n5. ���i�" << endl;
        if (error_input)
        {
            cout << endl << "����������� ������� �������!" << endl << endl;
            error_input = false;
        }
        cout << "�����i�� �������:\n\n>>";
        cin.clear();
        cin.sync();
        cin >> command;
        switch (command)
        {
        default:
            error_input = true;
            continue;
        case 1:
            system("cls");
            database->addOrderedProduct();
            break;
        case 2:
            {
                int number;
                do
                {
                    cout << "����i�� ����� �������� ���� �����i��� ����������: " << endl;
                }
                while((cin >> number) && ((number < 1) || ((unsigned int)number > database->tableOderedProduct.size())));
                database->editOrderedProduct(number - 1);
            }
            break;
        case 3:
            {
                int number;
                do
                {
                    cout << "����i�� ����� �������� ���� �����i��� ��������: " << endl;
                }
                while((cin >> number) && ((number < 1) || ((unsigned int)number > database->tableOderedProduct.size())));
                database->deleteOrderedProduct(number - 1);
            }
            break;
        case 4:
            {
                system("cls");
                int temp1 = 0;
                int temp2 = 0;
                cout << "� �������:\n1. ���������\n2. ��������" << endl;
                while ((temp1 != 1) && (temp1 != 2))
                {
                    cout << "�����i�� �������:\n\n>>";
                    cin >> temp1;
                }
                system("cls");
                cout << "��������� ��:" << endl;
                for (unsigned int i = 0; i < database->myConfig[5].name_pole.size(); i++)
                    cout << i + 1 << ". " << database->myConfig[5].name_pole[i] << endl;
                while ((temp2 < 1) || ((unsigned int)temp1 > database->myConfig[5].name_pole.size()))
                {
                    cout << "�����i�� �������:\n\n>>";
                    cin >> temp2;
                }
                database->sortOrderedProduct(temp2);
                if (temp1 == 2)
                    reverse(database->tableOderedProduct.begin(), database->tableOderedProduct.end());

            }
            break;
        case 5:
            exit_prog = true;
            break;
        }
    }
}

void Menu::workProduct()
{
    bool exit_prog = false;
    bool error_input = false;
    int command;
    while (!exit_prog)
    {
        system("cls");
        cout << "�������: " << database->myConfig[6].name << endl;
        for (unsigned int i = 0; i < database->myConfig[6].name_pole.size(); i++)
        {
            cout << setw(23) << database->myConfig[6].name_pole[i] << setw(23);
        }
        cout << endl;
        database->getScreenProduct();
        cout << "\n\n";
        cout << "1. ������ �������\n2. ���������� �������\n3. �������� �������\n4. ����������\n\n5. ���i�" << endl;
        if (error_input)
        {
            cout << endl << "����������� ������� �������!" << endl << endl;
            error_input = false;
        }
        cout << "�����i�� �������:\n\n>>";
        cin.clear();
        cin.sync();
        cin >> command;
        switch (command)
        {
        default:
            error_input = true;
            continue;
        case 1:
            system("cls");
            database->addProduct();
            break;
        case 2:
            {
                int number;
                do
                {
                    cout << "����i�� ����� �������� ���� �����i��� ����������: " << endl;
                }
                while((cin >> number) && ((number < 1) || ((unsigned int)number > database->tableProduct.size())));
                database->editProduct(number - 1);
            }
            break;
        case 3:
            {
                int number;
                do
                {
                    cout << "����i�� ����� �������� ���� �����i��� ��������: " << endl;
                }
                while((cin >> number) && ((number < 1) || ((unsigned int)number > database->tableProduct.size())));
                database->deleteProduct(number - 1);
            }
            break;
        case 4:
            {
                system("cls");
                int temp1 = 0;
                int temp2 = 0;
                cout << "� �������:\n1. ���������\n2. ��������" << endl;
                while ((temp1 != 1) && (temp1 != 2))
                {
                    cout << "�����i�� �������:\n\n>>";
                    cin >> temp1;
                }
                system("cls");
                cout << "��������� ��:" << endl;
                for (unsigned int i = 0; i < database->myConfig[6].name_pole.size(); i++)
                    cout << i + 1 << ". " << database->myConfig[6].name_pole[i] << endl;
                while ((temp2 < 1) || ((unsigned int)temp1 > database->myConfig[6].name_pole.size()))
                {
                    cout << "�����i�� �������:\n\n>>";
                    cin >> temp2;
                }
                database->sortProduct(temp2);
                if (temp1 == 2)
                    reverse(database->tableProduct.begin(), database->tableProduct.end());

            }
            break;
        case 5:
            exit_prog = true;
            break;
        }
    }
}

void Menu::workTypeProduct()
{
    bool exit_prog = false;
    bool error_input = false;
    int command;
    while (!exit_prog)
    {
        system("cls");
        cout << "�������: " << database->myConfig[7].name << endl;
        for (unsigned int i = 0; i < database->myConfig[7].name_pole.size(); i++)
        {
            cout << setw(45) << database->myConfig[7].name_pole[i] << setw(45);
        }
        cout << endl;
        database->getScreenTypeProduct();
        cout << "\n\n";
        cout << "1. ������ �������\n2. ���������� �������\n3. �������� �������\n4. ����������\n\n5. ���i�" << endl;
        if (error_input)
        {
            cout << endl << "����������� ������� �������!" << endl << endl;
            error_input = false;
        }
        cout << "�����i�� �������:\n\n>>";
        cin.clear();
        cin.sync();
        cin >> command;
        switch (command)
        {
        default:
            error_input = true;
            continue;
        case 1:
            system("cls");
            database->addTypeProduct();
            break;
        case 2:
            {
                int number;
                do
                {
                    cout << "����i�� ����� �������� ���� �����i��� ����������: " << endl;
                }
                while((cin >> number) && ((number < 1) || ((unsigned int)number > database->tableTypeProduct.size())));
                database->editTypeProduct(number - 1);
            }
            break;
        case 3:
            {
                int number;
                do
                {
                    cout << "����i�� ����� �������� ���� �����i��� ��������: " << endl;
                }
                while((cin >> number) && ((number < 1) || ((unsigned int)number > database->tableTypeProduct.size())));
                database->deleteTypeProduct(number - 1);
            }
            break;
        case 4:
            {
                system("cls");
                int temp1 = 0;
                int temp2 = 0;
                cout << "� �������:\n1. ���������\n2. ��������" << endl;
                while ((temp1 != 1) && (temp1 != 2))
                {
                    cout << "�����i�� �������:\n\n>>";
                    cin >> temp1;
                }
                system("cls");
                cout << "��������� ��:" << endl;
                for (unsigned int i = 0; i < database->myConfig[7].name_pole.size(); i++)
                    cout << i + 1 << ". " << database->myConfig[7].name_pole[i] << endl;
                while ((temp2 < 1) || ((unsigned int)temp1 > database->myConfig[7].name_pole.size()))
                {
                    cout << "�����i�� �������:\n\n>>";
                    cin >> temp2;
                }
                database->sortTypeProduct(temp2);
                if (temp1 == 2)
                    reverse(database->tableTypeProduct.begin(), database->tableTypeProduct.end());

            }
            break;
        case 5:
            exit_prog = true;
            break;
        }
    }
}

