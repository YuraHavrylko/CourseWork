#include "Database.h"

Database::Database()
{

}

Database::~Database()
{

}

void Database::clearArrays()
{
    tableCustomers.clear();
    tableDelivery.clear();
    tableEmployees.clear();
    tableManufacturerProduct.clear();
    tableOderedProduct.clear();
    tableOrder.clear();
    tableProduct.clear();
    tableTypeProduct.clear();
    for (int i = 0; i < 8; i++)
        myConfig[i].name_pole.clear();
}

void Database::openFile()
{
    bool exit = false;
    while (!exit)
    {
        system("cls");
        cout << "����i�� ����� ���� �����, ���� �����i��� �i������:" << endl;
        cin >> nameFile;
        try
        {
            DBFile.close();
            configFile.close();
            DBFile.open(nameFile);
            configFile.open("Config");
            if (DBFile.fail() || configFile.fail())
                throw 0;
            cout << "���� ����� �i�����!" << endl;
            exit = true;
            cin.get();
            cin.get();
        }
        catch(...)
        {
            cout << "������� ��� �i������i ���� �����!" << endl;
            cin.get();
            cin.get();
        }
    }
}

void Database::createFile()
{
    system("cls");
    cout << "����i�� ����� ���� �����, ���� �����i��� ��������:" << endl;
    cin >> nameFile;
    ofstream file(nameFile);
    for (int i = 0; i < 8; i++)
        file << "0" << endl;
    file.close();
    cout << "���� ����� ��������!" << endl;
    cin.get();
    cin.get();
}
void Database::deleteFile()
{
    system("cls");
    cout << "����i�� ����� ���� �����, ���� �����i��� ��������:" << endl;
    cin >> nameFile;
    remove(nameFile.c_str());
    cout << "���� ����� ��������!" << endl;
    cin.get();
    cin.get();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////

void Database::loadDataBase()
{
    for (int i = 0; i < 8; i++) // ������� ������������ ���� �����
    {
        int temp;
        configFile >> myConfig[i].name >> temp;
        for (int j = 0; j < temp; j++)
        {
            string s;
            configFile >> s;
            myConfig[i].name_pole.push_back(s);
        }
    }
    loadFileCustomers();
    loadFileDelivery();
    loadFileEmployees();
    loadFileManufacturerProduct();
    loadFileOrder();
    loadFileOrderedProduct();
    loadFileProduct();
    loadFileTypeProduct();
}

void Database::loadFileCustomers()
{
    int arg;
    DBFile >> arg;
    for (int i = 0; i < arg; i++)
    {
        customers obj;
        string s1, s2, s3, s4, s5, s6;
        DBFile >> s1 >> s2 >> s3 >> s4 >> s5 >> s6;
        obj.Sid = s1;
        obj.Sname = s2;
        obj.Sadres = s3;
        obj.Scity = s4;
        obj.Scountry = s5;
        obj.Stelephone = s6;
        tableCustomers.push_back(obj);
    }
}

void Database::loadFileDelivery()
{
    int arg;
    DBFile >> arg;
    for (int i = 0; i < arg; i++)
    {
        delivery obj;
        int i1;
        string s2, s3;
        DBFile >> i1 >> s2 >> s3;
        obj.Iid = i1;
        obj.Sname = s2;
        obj.Stelephone = s3;
        tableDelivery.push_back(obj);
    }
}
void Database::loadFileEmployees()
{
    int arg;
    DBFile >> arg;
    for (int i = 0; i < arg; i++)
    {
        employees obj;
        int i1;
        string s2, s3, s4, s5, s6, s7, s8;
        DBFile >> i1 >> s2 >> s3 >> s4 >> s5 >> s6 >> s7 >> s8;
        obj.Iid = i1;
        obj.Sfirst_name = s2;
        obj.Slast_name = s3;
        obj.Sposition = s4;
        obj.Sdata = s5;
        obj.Sadress = s6;
        obj.Scity = s7;
        obj.Stelephone = s8;
        tableEmployees.push_back(obj);
    }
}
void Database::loadFileManufacturerProduct()
{
    int arg;
    DBFile >> arg;
    for (int i = 0; i < arg; i++)
    {
        manufacturerProduct obj;
        int i1;
        string s2, s3, s4, s5, s6, s7;
        DBFile >> i1 >> s2 >> s3 >> s4 >> s5 >> s6 >> s7;
        obj.Iid = i1;
        obj.Sname = s2;
        obj.Sadress = s3;
        obj.Scity = s4;
        obj.Scountry = s5;
        obj.Stelephone = s6;
        obj.Shome_page = s7;
        tableManufacturerProduct.push_back(obj);
    }
}

void Database::loadFileOrder()
{
    int arg;
    DBFile >> arg;
    for (int i = 0; i < arg; i++)
    {
        order obj;
        int i1;
        double d7;
        string s2, s3, s4, s5, s6, s8, s9, s10, s11;
        DBFile >> i1 >> s2 >> s3 >> s4 >> s5 >> s6 >> d7 >> s8 >> s9 >> s10 >> s11;
        obj.Iid = i1;
        obj.Sclient = s2;
        obj.Semployees = s3;
        obj.Sdate_of_appointment = s4;
        obj.Sexecution_date = s5;
        obj.Sdelivery = s6;
        obj.Dcost_delivery = d7;
        obj.Sname_recipient = s8;
        obj.Sadress_recipient = s9;
        obj.Scity_recipient = s10;
        obj.Scountry_recipient = s11;
        tableOrder.push_back(obj);
    }
}
void Database::loadFileOrderedProduct()
{
    int arg;
    DBFile >> arg;
    for (int i = 0; i < arg; i++)
    {
        orderedProduct obj;
        int i1, i4;
        double d3;
        string s2;
        DBFile >> i1 >> s2 >> d3 >> i4;
        obj.Iid = i1;
        obj.Sproduct = s2;
        obj.Dprice = d3;
        obj.Iamount = i4;
        tableOderedProduct.push_back(obj);
    }
}

void Database::loadFileProduct()
{
    int arg;
    DBFile >> arg;
    for (int i = 0; i < arg; i++)
    {
        product obj;
        int i1 ,i6;
        double d5;
        string s2, s3, s4, s7, s8;
        DBFile >> i1 >> s2 >> s3 >> s4 >> d5 >> i6 >> s7 >> s8;
        obj.Iid = i1;
        obj.Stype = s2;
        obj.Smanufacturer = s3;
        obj.Sbrand = s4;
        obj.Dprice = d5;
        obj.Iamount = i6;
        obj.Sunit_of_measurement = s7;
        obj.Ssupply = s8;
        tableProduct.push_back(obj);
    }
}
void Database::loadFileTypeProduct()
{
    int arg;
    DBFile >> arg;
    for (int i = 0; i < arg; i++)
    {
        typeProduct obj;
        int i1;
        string s2, s3;
        DBFile >> i1 >> s2 >> s3;
        obj.Iid = i1;
        obj.Scategory = s2;
        obj.Sorder = s3;
        tableTypeProduct.push_back(obj);
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////

void Database::getScreenCustomers()
{
    cout << '\n';
    for(unsigned int i = 0; i < tableCustomers.size(); i++)
    {
        cout << i + 1;
        cout << setw(25) << tableCustomers[i].Sid << setw(25);
        cout << setw(25) << tableCustomers[i].Sname << setw(25);
        cout << setw(25) << tableCustomers[i].Sadres << setw(25);
        cout << setw(25) << tableCustomers[i].Scity << setw(25);
        cout << setw(25) << tableCustomers[i].Scountry << setw(25);
        cout << setw(25) << tableCustomers[i].Stelephone;
        cout << '\n';
    }
}

void Database::getScreenDelivery()
{
    cout << '\n';
    for(unsigned int i = 0; i < tableDelivery.size(); i++)
    {
        cout << i + 1;
        cout << setw(25) << tableDelivery[i].Iid << setw(25);
        cout << setw(25) << tableDelivery[i].Sname << setw(25);
        cout << setw(25) << tableDelivery[i].Stelephone << setw(25);
        cout << '\n';
    }
}

void Database::getScreenEmployees()
{
    cout << '\n';
    for(unsigned int i = 0; i < tableEmployees.size(); i++)
    {
        cout << i + 1;
        cout << setw(23) << tableEmployees[i].Iid << setw(23);
        cout << setw(23) << tableEmployees[i].Sfirst_name << setw(23);
        cout << setw(23) << tableEmployees[i].Slast_name << setw(23);
        cout << setw(23) << tableEmployees[i].Sposition << setw(23);
        cout << setw(23) << tableEmployees[i].Sdata << setw(23);
        cout << setw(23) << tableEmployees[i].Sadress << setw(23);
        cout << setw(23) << tableEmployees[i].Scity << setw(23);
        cout << setw(23) << tableEmployees[i].Stelephone << setw(23);
        cout << '\n';
    }
}

void Database::getScreenManufacturerProduct()
{
    cout << '\n';
    for(unsigned int i = 0; i < tableManufacturerProduct.size(); i++)
    {
        cout << i + 1;
        cout << setw(25) << tableManufacturerProduct[i].Iid << setw(25);
        cout << setw(25) << tableManufacturerProduct[i].Sname << setw(25);
        cout << setw(25) << tableManufacturerProduct[i].Sadress << setw(25);
        cout << setw(25) << tableManufacturerProduct[i].Scity << setw(25);
        cout << setw(25) << tableManufacturerProduct[i].Scountry << setw(25);
        cout << setw(25) << tableManufacturerProduct[i].Stelephone << setw(25);
        cout << setw(25) << tableManufacturerProduct[i].Shome_page << setw(25);
        cout << '\n';
    }
}

void Database::getScreenOrder()
{
    cout << '\n';
    for(unsigned int i = 0; i < tableOrder.size(); i++)
    {
        cout << i + 1;
        cout << setw(15) << tableOrder[i].Iid << setw(15);
        cout << setw(15) << tableOrder[i].Sclient << setw(15);
        cout << setw(15) << tableOrder[i].Semployees << setw(15);
        cout << setw(15) << tableOrder[i].Sdate_of_appointment << setw(15);
        cout << setw(15) << tableOrder[i].Sexecution_date << setw(15);
        cout << setw(15) << tableOrder[i].Sdelivery << setw(15);
        cout << setw(15) << tableOrder[i].Dcost_delivery << setw(15);
        cout << setw(15) << tableOrder[i].Sname_recipient << setw(15);
        cout << setw(15) << tableOrder[i].Sadress_recipient << setw(15);
        cout << setw(15) << tableOrder[i].Scity_recipient << setw(15);
        cout << setw(15) << tableOrder[i].Scountry_recipient << setw(15);
        cout << '\n';
    }
}

void Database::getScreenOrderedProduct()
{
    cout << '\n';
    for(unsigned int i = 0; i < tableOderedProduct.size(); i++)
    {
        cout << i + 1;
        cout << setw(25) << tableOderedProduct[i].Iid << setw(25);
        cout << setw(25) << tableOderedProduct[i].Sproduct << setw(25);
        cout << setw(25) << tableOderedProduct[i].Dprice << setw(25);
        cout << setw(25) << tableOderedProduct[i].Iamount << setw(25);
        cout << '\n';
    }
}

void Database::getScreenProduct()
{
    cout << '\n';
    for(unsigned int i = 0; i < tableProduct.size(); i++)
    {
        cout << i + 1;
        cout << setw(23) << tableProduct[i].Iid << setw(23);
        cout << setw(23) << tableProduct[i].Stype << setw(23);
        cout << setw(23) << tableProduct[i].Smanufacturer << setw(23);
        cout << setw(23) << tableProduct[i].Sbrand << setw(23);
        cout << setw(23) << tableProduct[i].Dprice << setw(23);
        cout << setw(23) << tableProduct[i].Iamount << setw(23);
        cout << setw(23) << tableProduct[i].Sunit_of_measurement << setw(23);
        cout << setw(23) << tableProduct[i].Ssupply << setw(23);
        cout << '\n';
    }
}

void Database::getScreenTypeProduct()
{
    cout << '\n';
    for(unsigned int i = 0; i < tableTypeProduct.size(); i++)
    {
        cout << i + 1;
        cout << setw(45) << tableTypeProduct[i].Iid << setw(45);
        cout << setw(45) << tableTypeProduct[i].Scategory << setw(45);
        cout << setw(45) << tableTypeProduct[i].Sorder << setw(45);
        cout << '\n';
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

void Database::saveDataBase()
{
    DBFile.close();
    DBFile.open("DB");
    saveFileCustomers();
    saveFileDelivery();
    saveFileEmployees();
    saveFileManufacturerProduct();
    saveFileOrder();
    saveFileOrderedProduct();
    saveFileProduct();
    saveFileTypeProduct();
}


void Database::saveFileCustomers()
{
    DBFile << tableCustomers.size() << endl;
    for(unsigned int i = 0; i < tableCustomers.size(); i++)
    {
        DBFile << tableCustomers[i].Sid << " ";
        DBFile << tableCustomers[i].Sname << " ";
        DBFile << tableCustomers[i].Sadres << " ";
        DBFile << tableCustomers[i].Scity << " ";
        DBFile << tableCustomers[i].Scountry << " ";
        DBFile << tableCustomers[i].Stelephone << " ";
        DBFile << endl;
    }
}

void Database::saveFileDelivery()
{
    DBFile << tableDelivery.size() << endl;
    for(unsigned int i = 0; i < tableDelivery.size(); i++)
    {
        DBFile << tableDelivery[i].Iid << " ";
        DBFile << tableDelivery[i].Sname << " ";
        DBFile << tableDelivery[i].Stelephone << " ";
        DBFile << endl;
    }
}

void Database::saveFileEmployees()
{
    DBFile << tableEmployees.size() << endl;
    for(unsigned int i = 0; i < tableEmployees.size(); i++)
    {
        DBFile << tableEmployees[i].Iid << " ";
        DBFile << tableEmployees[i].Sfirst_name << " ";
        DBFile << tableEmployees[i].Slast_name << " ";
        DBFile << tableEmployees[i].Sposition << " ";
        DBFile << tableEmployees[i].Sdata << " ";
        DBFile << tableEmployees[i].Sadress << " ";
        DBFile << tableEmployees[i].Scity << " ";
        DBFile << tableEmployees[i].Stelephone << " ";
        DBFile << endl;
    }
}

void Database::saveFileManufacturerProduct()
{
    DBFile << tableManufacturerProduct.size() << endl;
    for(unsigned int i = 0; i < tableManufacturerProduct.size(); i++)
    {
        DBFile << tableManufacturerProduct[i].Iid << " ";
        DBFile << tableManufacturerProduct[i].Sname << " ";
        DBFile << tableManufacturerProduct[i].Sadress << " ";
        DBFile << tableManufacturerProduct[i].Scity << " ";
        DBFile << tableManufacturerProduct[i].Scountry << " ";
        DBFile << tableManufacturerProduct[i].Stelephone << " ";
        DBFile << tableManufacturerProduct[i].Shome_page << " ";
        DBFile << endl;
    }
}


void Database::saveFileOrder()
{
    DBFile << tableOrder.size() << endl;
    for(unsigned int i = 0; i < tableOrder.size(); i++)
    {
        DBFile << tableOrder[i].Iid << " ";
        DBFile << tableOrder[i].Sclient << " ";
        DBFile << tableOrder[i].Semployees << " ";
        DBFile << tableOrder[i].Sdate_of_appointment << " ";
        DBFile << tableOrder[i].Sexecution_date << " ";
        DBFile << tableOrder[i].Sdelivery << " ";
        DBFile << tableOrder[i].Dcost_delivery << " ";
        DBFile << tableOrder[i].Sname_recipient << " ";
        DBFile << tableOrder[i].Sadress_recipient << " ";
        DBFile << tableOrder[i].Scity_recipient << " ";
        DBFile << tableOrder[i].Scountry_recipient << " ";
        DBFile << endl;
    }
}

void Database::saveFileOrderedProduct()
{
    DBFile << tableOderedProduct.size() << endl;
    for(unsigned int i = 0; i < tableOderedProduct.size(); i++)
    {
        DBFile << tableOderedProduct[i].Iid << " ";
        DBFile << tableOderedProduct[i].Sproduct << " ";
        DBFile << tableOderedProduct[i].Dprice << " ";
        DBFile << tableOderedProduct[i].Iamount << " ";
        DBFile << endl;
    }
}

void Database::saveFileProduct()
{
    DBFile << tableProduct.size() << endl;
    for(unsigned int i = 0; i < tableProduct.size(); i++)
    {
        DBFile << tableProduct[i].Iid << " ";
        DBFile << tableProduct[i].Stype << " ";
        DBFile << tableProduct[i].Smanufacturer << " ";
        DBFile << tableProduct[i].Sbrand << " ";
        DBFile << tableProduct[i].Dprice << " ";
        DBFile << tableProduct[i].Iamount << " ";
        DBFile << tableProduct[i].Sunit_of_measurement << " ";
        DBFile << tableProduct[i].Ssupply << " ";
        DBFile << endl;
    }
}

void Database::saveFileTypeProduct()
{
    DBFile << tableTypeProduct.size() << endl;
    for(unsigned int i = 0; i < tableTypeProduct.size(); i++)
    {
        DBFile << tableTypeProduct[i].Iid << " ";
        DBFile << tableTypeProduct[i].Scategory << " ";
        DBFile << tableTypeProduct[i].Sorder << " ";
        DBFile << endl;
    }

}
/////////////////////////////////////////////////////////////////////////////////////////////////////
void Database::addCustomers()
{
    string s[6];
    cout << "����� ������� ������i: " << myConfig[0].name << endl << endl;
    cout << "����i�� ����� ����i�: \n\n";
    for (unsigned int i = 0; i < myConfig[0].name_pole.size(); i++)
    {
        cout << myConfig[0].name_pole[i] << ": ";
        cin.sync();
        getline(cin, s[i]);
        cout << endl;
    }
    cout << endl;
    customers obj;
    obj.Sid = s[0];
    obj.Sname = s[1];
    obj.Sadres = s[2];
    obj.Scity = s[3];
    obj.Scountry = s[4];
    obj.Stelephone = s[5];
    tableCustomers.push_back(obj);
}

void Database::addDelivery()
{
    cout << "����� ������� ������i: " << myConfig[1].name << endl << endl;
    cout << "����i�� ����� ����i�: \n\n";
    int a;
    string s[2];
    for (unsigned int i = 0; i < myConfig[1].name_pole.size(); i++)
    {
        cout << myConfig[1].name_pole[i] << ": ";
        cin.sync();
        if (i)
            getline(cin, s[i - 1]);
        else
            cin >> a;
        cout << endl;
    }
    cout << endl;
    delivery obj;
    obj.Iid = a;
    obj.Sname = s[0];
    obj.Stelephone = s[1];
    tableDelivery.push_back(obj);
}

void Database::addEmployees()
{
    cout << "����� ������� ������i: " << myConfig[2].name << endl << endl;
    cout << "����i�� ����� ����i�: \n\n";
    int a;
    string s[7];
    for (unsigned int i = 0; i < myConfig[2].name_pole.size(); i++)
    {
        cout << myConfig[2].name_pole[i] << ": ";
        cin.sync();
        if (i)
            getline(cin, s[i - 1]);
        else
            cin >> a;
        cout << endl;
    }
    cout << endl;
    employees obj;
    obj.Iid = a;
    obj.Sfirst_name = s[0];
    obj.Slast_name = s[1];
    obj.Sposition = s[2];
    obj.Sdata = s[3];
    obj.Sadress = s[4];
    obj.Scity = s[5];
    obj.Stelephone = s[6];
    tableEmployees.push_back(obj);
}

void Database::addManufacturerProduct()
{
    cout << "����� ������� ������i: " << myConfig[3].name << endl << endl;
    cout << "����i�� ����� ����i�: \n\n";
    int a;
    string s[7];
    for (unsigned int i = 0; i < myConfig[3].name_pole.size(); i++)
    {
        cout << myConfig[3].name_pole[i] << ": ";
        cin.sync();
        if (i)
            getline(cin, s[i]);
        else
            cin >> a;
        cout << endl;
    }
    cout << endl;
    manufacturerProduct obj;
    obj.Iid = a;
    obj.Sname = s[1];
    obj.Sadress = s[2];
    obj.Scity = s[3];
    obj.Scountry = s[4];
    obj.Stelephone = s[5];
    obj.Shome_page = s[6];
    tableManufacturerProduct.push_back(obj);
}

void Database::addOrder()
{
    cout << "����� ������� ������i: " << myConfig[4].name << endl << endl;
    cout << "����i�� ����� ����i�: \n\n";
    int a;
    double b;
    string s[11];
    for (unsigned int i = 0; i < myConfig[4].name_pole.size(); i++)
    {
        cout << myConfig[4].name_pole[i] << ": ";
        cin.sync();
        if (i && (i != 6))
            getline(cin, s[i]);
        else if (i == 0)
            cin >> a;
        else
            cin >> b;
        cout << endl;
    }
    cout << endl;
    order obj;
    obj.Iid = a;
    obj.Sclient = s[1];
    obj.Semployees = s[2];
    obj.Sdate_of_appointment = s[3];
    obj.Sexecution_date = s[4];
    obj.Sdelivery = s[5];
    obj.Dcost_delivery = b;
    obj.Sname_recipient = s[7];
    obj.Sadress_recipient = s[8];
    obj.Scity_recipient = s[9];
    obj.Scountry_recipient = s[10];
    tableOrder.push_back(obj);
}

void Database::addOrderedProduct()
{
    cout << "����� ������� ������i: " << myConfig[5].name << endl << endl;
    cout << "����i�� ����� ����i�: \n\n";
    int a, b;
    double c;
    string s;
    for (unsigned int i = 0; i < myConfig[5].name_pole.size(); i++)
    {
        cout << myConfig[5].name_pole[i] << ": ";
        cin.sync();
        if (i == 1)
            getline(cin, s);
        else if (i == 0)
            cin >> a;
        else if (i == 2)
            cin >> c;
        else
            cin >> b;
        cout << endl;
    }
    orderedProduct obj;
    obj.Iid = a;
    obj.Sproduct = s;
    obj.Dprice = c;
    obj.Iamount = b;
    tableOderedProduct.push_back(obj);
}

void Database::addProduct()
{
    cout << "����� ������� ������i: " << myConfig[6].name << endl << endl;
    cout << "����i�� ����� ����i�: \n\n";
    int a, b;
    double c;
    string s[8];
    for (unsigned int i = 0; i < myConfig[6].name_pole.size(); i++)
    {
        cout << myConfig[6].name_pole[i] << ": ";
        cin.sync();
        if (i == 0)
            cin >> a;
        else if (i == 4)
            cin >> c;
        else if (i == 5)
            cin >> b;
        else
            getline(cin, s[i]);
        cout << endl;
    }
    product obj;
    obj.Iid = a;
    obj.Stype = s[1];
    obj.Smanufacturer = s[2];
    obj.Sbrand = s[3];
    obj.Dprice = c;
    obj.Iamount = b;
    obj.Sunit_of_measurement = s[6];
    obj.Ssupply = s[7];
    tableProduct.push_back(obj);
}

void Database::addTypeProduct()
{
    cout << "����� ������� ������i: " << myConfig[7].name << endl << endl;
    cout << "����i�� ����� ����i�: \n\n";
    int a;
    string s[2];
    for (unsigned int i = 0; i < myConfig[7].name_pole.size(); i++)
    {
        cout << myConfig[7].name_pole[i] << ": ";
        cin.sync();
        if (i)
            getline(cin, s[i - 1]);
        else
            cin >> a;
        cout << endl;
    }
    cout << endl;
    typeProduct obj;
    obj.Iid = a;
    obj.Scategory = s[0];
    obj.Sorder = s[1];
    tableTypeProduct.push_back(obj);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

void Database::editCustomers(int temp)
{
    cout << "����������� �������� ������i: " << myConfig[0].name << endl << endl;
    cout << "��������i ���i: \n\n";
    for (unsigned int i = 0; i < myConfig[0].name_pole.size(); i++)
    {
        cout << setw(25) << myConfig[0].name_pole[i] << setw(25);
    }
    cout << endl;
    cout << setw(25) << tableCustomers[temp].Sid << setw(25);
    cout << setw(25) << tableCustomers[temp].Sname << setw(25);
    cout << setw(25) << tableCustomers[temp].Sadres << setw(25);
    cout << setw(25) << tableCustomers[temp].Scity << setw(25);
    cout << setw(25) << tableCustomers[temp].Scountry << setw(25);
    cout << setw(25) << tableCustomers[temp].Stelephone << setw(25);
    cout << endl;
    cout << "����i��: ";
    cout << endl;
    string s[6];
    for (unsigned int i = 0; i < myConfig[0].name_pole.size(); i++)
    {
        cout << myConfig[0].name_pole[i] << ": ";
        cin.sync();
        getline(cin, s[i]);
        cout << endl;
    }
    cout << endl;
    customers obj;
    obj.Sid = s[0];
    obj.Sname = s[1];
    obj.Sadres = s[2];
    obj.Scity = s[3];
    obj.Scountry = s[4];
    obj.Stelephone = s[5];
    tableCustomers[temp] = obj;
}

void Database::editDelivery(int temp)
{
    cout << "����������� �������� ������i: " << myConfig[1].name << endl << endl;
    cout << "��������i ���i: \n\n";
    for (unsigned int i = 0; i < myConfig[1].name_pole.size(); i++)
    {
        cout << setw(25) << myConfig[1].name_pole[i] << setw(25);
    }
    cout << endl;
    cout << setw(25) << tableDelivery[temp].Iid << setw(25);
    cout << setw(25) << tableDelivery[temp].Sname << setw(25);
    cout << setw(25) << tableDelivery[temp].Stelephone << setw(25);
    cout << endl << endl;
    cout << "����i��: ";
    cout << endl;
    int a;
    string s[2];
    for (unsigned int i = 0; i < myConfig[1].name_pole.size(); i++)
    {
        cout << myConfig[1].name_pole[i] << ": ";
        cin.sync();
        if (i)
            getline(cin, s[i - 1]);
        else
            cin >> a;
        cout << endl;
    }
    cout << endl;
    delivery obj;
    obj.Iid = a;
    obj.Sname = s[0];
    obj.Stelephone = s[1];
    tableDelivery[temp] = obj;
}

void Database::editEmployees(int temp)
{
    cout << "����������� �������� ������i: " << myConfig[2].name << endl << endl;
    cout << "��������i ���i: \n\n";
    for (unsigned int i = 0; i < myConfig[2].name_pole.size(); i++)
    {
        cout << setw(23) << myConfig[2].name_pole[i] << setw(23);
    }
    cout << endl;
    cout << setw(23) << tableEmployees[temp].Iid << setw(23);
    cout << setw(23) << tableEmployees[temp].Sfirst_name << setw(23);
    cout << setw(23) << tableEmployees[temp].Slast_name << setw(23);
    cout << setw(23) << tableEmployees[temp].Sposition << setw(23);
    cout << setw(23) << tableEmployees[temp].Sdata << setw(23);
    cout << setw(23) << tableEmployees[temp].Sadress << setw(23);
    cout << setw(23) << tableEmployees[temp].Scity << setw(23);
    cout << setw(23) << tableEmployees[temp].Stelephone << setw(23);
    cout << endl;
    cout << "����i��: ";
    cout << endl;
    int a;
    string s[7];
    for (unsigned int i = 0; i < myConfig[2].name_pole.size(); i++)
    {
        cout << myConfig[2].name_pole[i] << ": ";
        cin.sync();
        if (i)
            getline(cin, s[i - 1]);
        else
            cin >> a;
        cout << endl;
    }
    cout << endl;
    employees obj;
    obj.Iid = a;
    obj.Sfirst_name = s[0];
    obj.Slast_name = s[1];
    obj.Sposition = s[2];
    obj.Sdata = s[3];
    obj.Sadress = s[4];
    obj.Scity = s[5];
    obj.Stelephone = s[6];
    tableEmployees[temp] = obj;
}

void Database::editManufacturerProduct(int temp)
{
    cout << "����������� �������� ������i: " << myConfig[3].name << endl << endl;
    cout << "��������i ���i: \n\n";
    for (unsigned int i = 0; i < myConfig[3].name_pole.size(); i++)
    {
        cout << setw(25) << myConfig[0].name_pole[i] << setw(25);
    }
    cout << setw(25) << tableManufacturerProduct[temp].Iid << setw(25);
    cout << setw(25) << tableManufacturerProduct[temp].Sname << setw(25);
    cout << setw(25) << tableManufacturerProduct[temp].Sadress << setw(25);
    cout << setw(25) << tableManufacturerProduct[temp].Scity << setw(25);
    cout << setw(25) << tableManufacturerProduct[temp].Scountry << setw(25);
    cout << setw(25) << tableManufacturerProduct[temp].Stelephone << setw(25);
    cout << setw(25) << tableManufacturerProduct[temp].Shome_page << setw(25);
    cout << endl;
    cout << "����i��: ";
    cout << endl;
    int a;
    string s[7];
    for (unsigned int i = 0; i < myConfig[3].name_pole.size(); i++)
    {
        cout << myConfig[3].name_pole[i] << ": ";
        cin.sync();
        if (i)
            getline(cin, s[i]);
        else
            cin >> a;
        cout << endl;
    }
    cout << endl;
    manufacturerProduct obj;
    obj.Iid = a;
    obj.Sname = s[1];
    obj.Sadress = s[2];
    obj.Scity = s[3];
    obj.Scountry = s[4];
    obj.Stelephone = s[5];
    obj.Shome_page = s[6];
    tableManufacturerProduct[temp] = obj;
}

void Database::editOrder(int temp)
{
    cout << "����������� �������� ������i: " << myConfig[4].name << endl << endl;
    cout << "��������i ���i: \n\n";
    for (unsigned int i = 0; i < myConfig[4].name_pole.size(); i++)
    {
        cout << setw(15) << myConfig[4].name_pole[i] << setw(15);
    }
    cout << endl;
    cout << setw(15) << tableOrder[temp].Iid << setw(15);
    cout << setw(15) << tableOrder[temp].Sclient << setw(15);
    cout << setw(15) << tableOrder[temp].Semployees << setw(15);
    cout << setw(15) << tableOrder[temp].Sdate_of_appointment << setw(15);
    cout << setw(15) << tableOrder[temp].Sexecution_date << setw(15);
    cout << setw(15) << tableOrder[temp].Sdelivery << setw(15);
    cout << setw(15) << tableOrder[temp].Dcost_delivery << setw(15);
    cout << setw(15) << tableOrder[temp].Sname_recipient << setw(15);
    cout << setw(15) << tableOrder[temp].Sadress_recipient << setw(15);
    cout << setw(15) << tableOrder[temp].Scity_recipient << setw(15);
    cout << setw(15) << tableOrder[temp].Scountry_recipient << setw(15);
    cout << endl;
    cout << "����i��: ";
    cout << endl;
    int a;
    double b;
    string s[11];
    for (unsigned int i = 0; i < myConfig[4].name_pole.size(); i++)
    {
        cout << myConfig[4].name_pole[i] << ": ";
        cin.sync();
        if (i && (i != 6))
            getline(cin, s[i]);
        else if (i == 0)
            cin >> a;
        else
            cin >> b;
        cout << endl;
    }
    cout << endl;
    order obj;
    obj.Iid = a;
    obj.Sclient = s[1];
    obj.Semployees = s[2];
    obj.Sdate_of_appointment = s[3];
    obj.Sexecution_date = s[4];
    obj.Sdelivery = s[5];
    obj.Dcost_delivery = b;
    obj.Sname_recipient = s[7];
    obj.Sadress_recipient = s[8];
    obj.Scity_recipient = s[9];
    obj.Scountry_recipient = s[10];
    tableOrder[temp] = obj;
}

void Database::editOrderedProduct(int temp)
{
    cout << "����������� �������� ������i: " << myConfig[5].name << endl << endl;
    cout << "��������i ���i: \n\n";
    for (unsigned int i = 0; i < myConfig[5].name_pole.size(); i++)
    {
        cout << setw(25) << myConfig[5].name_pole[i] << setw(25);
    }
    cout << endl;
    cout << setw(25) << tableOderedProduct[temp].Iid << setw(25);
    cout << setw(25) << tableOderedProduct[temp].Sproduct << setw(25);
    cout << setw(25) << tableOderedProduct[temp].Dprice << setw(25);
    cout << setw(25) << tableOderedProduct[temp].Iamount << setw(25);
    cout << endl;
    cout << "����i��: ";
    cout << endl;
    int a, b;
    double c;
    string s;
    for (unsigned int i = 0; i < myConfig[5].name_pole.size(); i++)
    {
        cout << myConfig[5].name_pole[i] << ": ";
        cin.sync();
        if (i == 1)
            getline(cin, s);
        else if (i == 0)
            cin >> a;
        else if (i == 3)
            cin >> c;
        else
            cin >> b;
        cout << endl;
    }
    orderedProduct obj;
    obj.Iid = a;
    obj.Sproduct = s;
    obj.Dprice = c;
    obj.Iamount = b;
    tableOderedProduct[temp] = obj;
}

void Database::editProduct(int temp)
{
    cout << "����������� �������� ������i: " << myConfig[6].name << endl << endl;
    cout << "��������i ���i: \n\n";
    for (unsigned int i = 0; i < myConfig[6].name_pole.size(); i++)
    {
        cout << setw(23) << myConfig[6].name_pole[i] << setw(23);
    }
    cout << endl;
    cout << setw(23) << tableProduct[temp].Iid << setw(23);
    cout << setw(23) << tableProduct[temp].Stype << setw(23);
    cout << setw(23) << tableProduct[temp].Smanufacturer << setw(23);
    cout << setw(23) << tableProduct[temp].Sbrand << setw(23);
    cout << setw(23) << tableProduct[temp].Dprice << setw(23);
    cout << setw(23) << tableProduct[temp].Iamount << setw(23);
    cout << setw(23) << tableProduct[temp].Sunit_of_measurement << setw(23);
    cout << setw(23) << tableProduct[temp].Ssupply << setw(23);
    cout << endl;
    cout << "����i��: ";
    cout << endl;
    int a, b;
    double c;
    string s[8];
    for (unsigned int i = 0; i < myConfig[6].name_pole.size(); i++)
    {
        cout << myConfig[6].name_pole[i] << ": ";
        cin.sync();
        if (i == 0)
            cin >> a;
        else if (i == 4)
            cin >> c;
        else if (i == 5)
            cin >> b;
        else
            getline(cin, s[i]);
        cout << endl;
    }
    product obj;
    obj.Iid = a;
    obj.Stype = s[1];
    obj.Smanufacturer = s[2];
    obj.Sbrand = s[3];
    obj.Dprice = c;
    obj.Iamount = b;
    obj.Sunit_of_measurement = s[6];
    obj.Ssupply = s[7];
    tableProduct[temp] = obj;
}

void Database::editTypeProduct(int temp)
{
    cout << "����������� �������� ������i: " << myConfig[7].name << endl << endl;
    cout << "��������i ���i: \n\n";
    for (unsigned int i = 0; i < myConfig[7].name_pole.size(); i++)
    {
        cout << setw(45) << myConfig[7].name_pole[i] << setw(45);
    }
    cout << endl;
    cout << setw(45) << tableTypeProduct[temp].Iid << setw(45);
    cout << setw(45) << tableTypeProduct[temp].Scategory << setw(45);
    cout << setw(45) << tableTypeProduct[temp].Sorder << setw(45);
    cout << endl;
    cout << "����i��: ";
    cout << endl;
    int a;
    string s[2];
    for (unsigned int i = 0; i < myConfig[7].name_pole.size(); i++)
    {
        cout << myConfig[7].name_pole[i] << ": ";
        cin.sync();
        if (i)
            getline(cin, s[i - 1]);
        else
            cin >> a;
        cout << endl;
    }
    cout << endl;
    typeProduct obj;
    obj.Iid = a;
    obj.Scategory = s[0];
    obj.Sorder = s[1];
    tableTypeProduct[temp] = obj;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

void Database::deleteCustomers(int temp)
{
    tableCustomers.erase(tableCustomers.begin() + temp);
}

void Database::deleteDelivery(int temp)
{
    tableDelivery.erase(tableDelivery.begin() + temp);
}

void Database::deleteEmployees(int temp)
{
    tableEmployees.erase(tableEmployees.begin() + temp);
}

void Database::deleteManufacturerProduct(int temp)
{
    tableManufacturerProduct.erase(tableManufacturerProduct.begin() + temp);
}

void Database::deleteOrder(int temp)
{
    tableOrder.erase(tableOrder.begin() + temp);
}

void Database::deleteOrderedProduct(int temp)
{
    tableOderedProduct.erase(tableOderedProduct.begin() + temp);
}

void Database::deleteProduct(int temp)
{
    tableProduct.erase(tableProduct.begin() + temp);
}

void Database::deleteTypeProduct(int temp)
{
    tableTypeProduct.erase(tableTypeProduct.begin() + temp);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

void Database::sortCustomers(int temp)
{
    switch(temp)
    {
    case 1:
        sort(tableCustomers.begin(), tableCustomers.end(), [](const customers& obj1, const customers& obj2) {return obj1.Sid < obj2.Sid;});
        break;
    case 2:
        sort(tableCustomers.begin(), tableCustomers.end(), [](const customers& obj1, const customers& obj2) {return obj1.Sname < obj2.Sname;});
        break;
    case 3:
        sort(tableCustomers.begin(), tableCustomers.end(), [](const customers& obj1, const customers& obj2) {return obj1.Sadres < obj2.Sadres;});
        break;
    case 4:
        sort(tableCustomers.begin(), tableCustomers.end(), [](const customers& obj1, const customers& obj2) {return obj1.Scity < obj2.Scity;});
        break;
    case 5:
        sort(tableCustomers.begin(), tableCustomers.end(), [](const customers& obj1, const customers& obj2) {return obj1.Scountry < obj2.Scountry;});
        break;
    case 6:
        sort(tableCustomers.begin(), tableCustomers.end(), [](const customers& obj1, const customers& obj2) {return obj1.Stelephone < obj2.Stelephone;});
        break;
    }
}

void Database::sortDelivery(int  temp)
{
    switch(temp)
    {
    case 1:
        sort(tableDelivery.begin(), tableDelivery.end(), [](const delivery& obj1, const delivery& obj2) {return obj1.Iid < obj2.Iid;});
        break;
    case 2:
        sort(tableDelivery.begin(), tableDelivery.end(), [](const delivery& obj1, const delivery& obj2) {return obj1.Sname < obj2.Sname;});
        break;
    case 3:
        sort(tableDelivery.begin(), tableDelivery.end(), [](const delivery& obj1, const delivery& obj2) {return obj1.Stelephone < obj2.Stelephone;});
        break;
    }
}

void Database::sortEmployees(int temp)
{
    switch(temp)
    {
    case 1:
        sort(tableEmployees.begin(), tableEmployees.end(), [](const employees& obj1, const employees& obj2) {return obj1.Iid < obj2.Iid;});
        break;
    case 2:
        sort(tableEmployees.begin(), tableEmployees.end(), [](const employees& obj1, const employees& obj2) {return obj1.Sfirst_name < obj2.Sfirst_name;});
        break;
    case 3:
        sort(tableEmployees.begin(), tableEmployees.end(), [](const employees& obj1, const employees& obj2) {return obj1.Slast_name < obj2.Slast_name;});
        break;
    case 4:
        sort(tableEmployees.begin(), tableEmployees.end(), [](const employees& obj1, const employees& obj2) {return obj1.Sposition < obj2.Sposition;});
        break;
    case 5:
        sort(tableEmployees.begin(), tableEmployees.end(), [](const employees& obj1, const employees& obj2) {return obj1.Sdata   < obj2.Sdata;});
        break;
    case 6:
        sort(tableEmployees.begin(), tableEmployees.end(), [](const employees& obj1, const employees& obj2) {return obj1.Sadress   < obj2.Sadress;});
        break;
    case 7:
        sort(tableEmployees.begin(), tableEmployees.end(), [](const employees& obj1, const employees& obj2) {return obj1.Scity   < obj2.Scity;});
        break;
    case 8:
        sort(tableEmployees.begin(), tableEmployees.end(), [](const employees& obj1, const employees& obj2) {return obj1.Stelephone   < obj2.Stelephone;});
        break;
    }
}

void Database::sortManufacturerProduct(int temp)
{
    switch(temp)
    {
    case 1:
        sort(tableManufacturerProduct.begin(), tableManufacturerProduct.end(), [](const manufacturerProduct& obj1, const manufacturerProduct& obj2) {return obj1.Iid < obj2.Iid;});
        break;
    case 2:
        sort(tableManufacturerProduct.begin(), tableManufacturerProduct.end(), [](const manufacturerProduct& obj1, const manufacturerProduct& obj2) {return obj1.Sname < obj2.Sname;});
        break;
    case 3:
        sort(tableManufacturerProduct.begin(), tableManufacturerProduct.end(), [](const manufacturerProduct& obj1, const manufacturerProduct& obj2) {return obj1.Sadress < obj2.Sadress;});
        break;
    case 4:
        sort(tableManufacturerProduct.begin(), tableManufacturerProduct.end(), [](const manufacturerProduct& obj1, const manufacturerProduct& obj2) {return obj1.Scity < obj2.Scity;});
        break;
    case 5:
        sort(tableManufacturerProduct.begin(), tableManufacturerProduct.end(), [](const manufacturerProduct& obj1, const manufacturerProduct& obj2) {return obj1.Scountry < obj2.Scountry;});
        break;
    case 6:
        sort(tableManufacturerProduct.begin(), tableManufacturerProduct.end(), [](const manufacturerProduct& obj1, const manufacturerProduct& obj2) {return obj1.Stelephone  < obj2.Stelephone ;});
        break;
    case 7:
        sort(tableManufacturerProduct.begin(), tableManufacturerProduct.end(), [](const manufacturerProduct& obj1, const manufacturerProduct& obj2) {return obj1.Shome_page  < obj2.Shome_page ;});
        break;
    }
}

void Database::sortOrder(int temp)
{
    switch(temp)
    {
    case 1:
        sort(tableOrder.begin(), tableOrder.end(), [](const order& obj1, const order& obj2) {return obj1.Iid < obj2.Iid;});
        break;
    case 2:
        sort(tableOrder.begin(), tableOrder.end(), [](const order& obj1, const order& obj2) {return obj1.Sclient < obj2.Sclient;});
        break;
    case 3:
        sort(tableOrder.begin(), tableOrder.end(), [](const order& obj1, const order& obj2) {return obj1.Semployees  < obj2.Semployees ;});
        break;
    case 4:
        sort(tableOrder.begin(), tableOrder.end(), [](const order& obj1, const order& obj2) {return obj1.Sdate_of_appointment  < obj2.Sdate_of_appointment ;});
        break;
    case 5:
        sort(tableOrder.begin(), tableOrder.end(), [](const order& obj1, const order& obj2) {return obj1.Sexecution_date  < obj2.Sexecution_date ;});
        break;
    case 6:
        sort(tableOrder.begin(), tableOrder.end(), [](const order& obj1, const order& obj2) {return obj1.Sdelivery < obj2.Sdelivery ;});
        break;
    case 7:
        sort(tableOrder.begin(), tableOrder.end(), [](const order& obj1, const order& obj2) {return obj1.Dcost_delivery  < obj2.Dcost_delivery ;});
        break;
    case 8:
        sort(tableOrder.begin(), tableOrder.end(), [](const order& obj1, const order& obj2) {return obj1.Sname_recipient  < obj2.Sname_recipient ;});
        break;
    case 9:
        sort(tableOrder.begin(), tableOrder.end(), [](const order& obj1, const order& obj2) {return obj1.Sadress_recipient  < obj2.Sadress_recipient ;});
        break;
    case 10:
        sort(tableOrder.begin(), tableOrder.end(), [](const order& obj1, const order& obj2) {return obj1.Scity_recipient  < obj2.Scity_recipient ;});
        break;
    case 11:
        sort(tableOrder.begin(), tableOrder.end(), [](const order& obj1, const order& obj2) {return obj1.Scountry_recipient  < obj2.Scountry_recipient ;});
        break;
    }
}

void Database::sortOrderedProduct(int temp)
{
    switch(temp)
    {
    case 1:
        sort(tableOderedProduct.begin(), tableOderedProduct.end(), [](const orderedProduct& obj1, const orderedProduct& obj2) {return obj1.Iid < obj2.Iid;});
        break;
    case 2:
        sort(tableOderedProduct.begin(), tableOderedProduct.end(), [](const orderedProduct& obj1, const orderedProduct& obj2) {return obj1.Sproduct < obj2.Sproduct;});
        break;
    case 3:
        sort(tableOderedProduct.begin(), tableOderedProduct.end(), [](const orderedProduct& obj1, const orderedProduct& obj2) {return obj1.Dprice  < obj2.Dprice ;});
        break;
    case 4:
        sort(tableOderedProduct.begin(), tableOderedProduct.end(), [](const orderedProduct& obj1, const orderedProduct& obj2) {return obj1.Iamount < obj2.Iamount;});
        break;
    }
}

void Database::sortProduct(int temp)
{
    switch(temp)
    {
    case 1:
        sort(tableProduct.begin(), tableProduct.end(), [](const product& obj1, const product& obj2) {return obj1.Iid < obj2.Iid;});
        break;
    case 2:
        sort(tableProduct.begin(), tableProduct.end(), [](const product& obj1, const product& obj2) {return obj1.Stype < obj2.Stype;});
        break;
    case 3:
        sort(tableProduct.begin(), tableProduct.end(), [](const product& obj1, const product& obj2) {return obj1.Smanufacturer  < obj2.Smanufacturer ;});
        break;
    case 4:
        sort(tableProduct.begin(), tableProduct.end(), [](const product& obj1, const product& obj2) {return obj1.Sbrand < obj2.Sbrand;});
        break;
    case 5:
        sort(tableProduct.begin(), tableProduct.end(), [](const product& obj1, const product& obj2) {return obj1.Dprice < obj2.Dprice;});
        break;
    case 6:
        sort(tableProduct.begin(), tableProduct.end(), [](const product& obj1, const product& obj2) {return obj1.Iamount < obj2.Iamount;});
        break;
    case 7:
        sort(tableProduct.begin(), tableProduct.end(), [](const product& obj1, const product& obj2) {return obj1.Sunit_of_measurement  < obj2.Sunit_of_measurement ;});
        break;
    case 8:
        sort(tableProduct.begin(), tableProduct.end(), [](const product& obj1, const product& obj2) {return obj1.Ssupply < obj2.Ssupply;});
        break;
    }
}

void Database::sortTypeProduct(int temp)
{
    switch(temp)
    {
    case 1:
        sort(tableTypeProduct.begin(), tableTypeProduct.end(), [](const typeProduct& obj1, const typeProduct& obj2) {return obj1.Iid < obj2.Iid;});
        break;
    case 2:
        sort(tableTypeProduct.begin(), tableTypeProduct.end(), [](const typeProduct& obj1, const typeProduct& obj2) {return obj1.Scategory < obj2.Scategory;});
        break;
    case 3:
        sort(tableTypeProduct.begin(), tableTypeProduct.end(), [](const typeProduct& obj1, const typeProduct& obj2) {return obj1.Sorder  < obj2.Sorder ;});
        break;
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////

void Database::queryPosition()
{
    vector <employees> emp;
    for (unsigned int i = 0; i < tableEmployees.size(); i++)
    {
        if (tableEmployees[i].Sposition == "�����������")
        {
            emp.push_back(tableEmployees[i]);
        }
    }
    for (unsigned int i = 0; i < myConfig[2].name_pole.size(); i++)
    {
        cout << setw(20) << myConfig[2].name_pole[i] << setw(20);
    }
    cout << endl;
    for (unsigned int temp = 0; temp < emp.size(); temp++)
    {
        cout << setw(20) << emp[temp].Iid << setw(20);
        cout << setw(20) << emp[temp].Sfirst_name << setw(20);
        cout << setw(20) << emp[temp].Slast_name << setw(20);
        cout << setw(20) << emp[temp].Sposition << setw(20);
        cout << setw(20) << emp[temp].Sdata << setw(20);
        cout << setw(20) << emp[temp].Sadress << setw(20);
        cout << setw(20) << emp[temp].Scity << setw(20);
        cout << setw(20) << emp[temp].Stelephone << setw(20);
        cout << endl;
    }
}

void Database::quetyPriceUpper()
{
    long double sum = 0;
    for(unsigned int i = 0; i < tableProduct.size(); i++)
        sum += tableProduct[i].Dprice;
    for (unsigned int i = 0; i < myConfig[6].name_pole.size(); i++)
    {
        cout << setw(23) << myConfig[6].name_pole[i] << setw(23);
    }
    cout << endl;
    sum = sum / (tableProduct.size());
    for(unsigned int i = 0; i < tableProduct.size(); i++)
    {
        if (sum < tableProduct[i].Dprice)
        {
            cout << setw(23) << tableProduct[i].Iid << setw(23);
            cout << setw(23) << tableProduct[i].Stype << setw(23);
            cout << setw(23) << tableProduct[i].Smanufacturer << setw(23);
            cout << setw(23) << tableProduct[i].Sbrand << setw(23);
            cout << setw(23) << tableProduct[i].Dprice << setw(23);
            cout << setw(23) << tableProduct[i].Iamount << setw(23);
            cout << setw(23) << tableProduct[i].Sunit_of_measurement << setw(23);
            cout << setw(23) << tableProduct[i].Ssupply << setw(23);
            cout << endl;
        }
    }
}


void Database::quetyProductIs()
{
    for (unsigned int i = 0; i < myConfig[6].name_pole.size(); i++)
    {
        cout << setw(23) << myConfig[6].name_pole[i] << setw(23);
    }
    cout << endl;
    for(unsigned int i = 0; i < tableProduct.size(); i++)
    {
        if (tableProduct[i].Ssupply == "��")
        {
            cout << setw(23) << tableProduct[i].Iid << setw(23);
            cout << setw(23) << tableProduct[i].Stype << setw(23);
            cout << setw(23) << tableProduct[i].Smanufacturer << setw(23);
            cout << setw(23) << tableProduct[i].Sbrand << setw(23);
            cout << setw(23) << tableProduct[i].Dprice << setw(23);
            cout << setw(23) << tableProduct[i].Iamount << setw(23);
            cout << setw(23) << tableProduct[i].Sunit_of_measurement << setw(23);
            cout << setw(23) << tableProduct[i].Ssupply << setw(23);
            cout << endl;
        }
    }
}

void Database::quetyIsPost()
{
    for (unsigned int i = 0; i < myConfig[4].name_pole.size(); i++)
    {
        cout << setw(15) << myConfig[4].name_pole[i] << setw(15);
    }
    cout << '\n';
    for(unsigned int i = 0; i < tableOrder.size(); i++)
    {
        if (tableOrder[i].Sdelivery == "�����")
        {
            cout << setw(15) << tableOrder[i].Iid << setw(15);
            cout << setw(15) << tableOrder[i].Sclient << setw(15);
            cout << setw(15) << tableOrder[i].Semployees << setw(15);
            cout << setw(15) << tableOrder[i].Sdate_of_appointment << setw(15);
            cout << setw(15) << tableOrder[i].Sexecution_date << setw(15);
            cout << setw(15) << tableOrder[i].Sdelivery << setw(15);
            cout << setw(15) << tableOrder[i].Dcost_delivery << setw(15);
            cout << setw(15) << tableOrder[i].Sname_recipient << setw(15);
            cout << setw(15) << tableOrder[i].Sadress_recipient << setw(15);
            cout << setw(15) << tableOrder[i].Scity_recipient << setw(15);
            cout << setw(15) << tableOrder[i].Scountry_recipient << setw(15);
            cout << '\n';
        }
    }
}
